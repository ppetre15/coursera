package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
    	if (c == r || c == 0 || r == 0) {
    		1
    	} else {
    		pascal(r-1, c) + pascal(r-1, c-1)
    	}
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
    	def balanceList(chars: List[Char], par: List[Char]): Boolean = {
    		if (chars.isEmpty) {
				par.isEmpty
    		} else if (chars.head == '(' ) {
    				balanceList(chars.tail , '(' :: par)
    		} else if (chars.head == ')' ) {
    				par.nonEmpty && balanceList(chars.tail, par.tail)
    		} else {
    			balanceList(chars.tail, par)
    		}
    	}
		balanceList(chars, List.empty[Char])
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = ???
  }
